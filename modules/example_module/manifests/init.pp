# descriptive comment!

class example_class {

  include other_module:class

  $interesting_variable = extlookup('key')

  file { "/home/${interesting_variable}":
    ensure => directory,
    owner  => $interesting_variable,
    mode   => 0777,
  }

  file { "/home/${interesting_variable}/filename"
    ensure  => file,
    owner   => $interesting_variable,
    mode    => 0700,
    source  => 'puppet:///modules/example_module/text_content',
  }

#example, cron job
  other_module::class::definition { 'parameter':
    command => '/bin/something',
    user    => $interesting_variable,
    minute  => 0,
  }

}
